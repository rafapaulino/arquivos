﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arquivos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FileInfo arquivo = new FileInfo(@"C:\diretorio\arquivo.txt");
            FileStream fs = arquivo.Create();
            MessageBox.Show("Criado na data e hora: " + arquivo.CreationTime);
            MessageBox.Show("Atributos: " + arquivo.Attributes.ToString());
            MessageBox.Show("Nome Completo: " + arquivo.FullName);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream(@"C:\diretorio\arquivo.txt",FileMode.OpenOrCreate,FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine("Mostrando o uso de arquivos");
            sw.WriteLine("O Ademir estava cagando...");
            sw.WriteLine(richTextBox1.Text);
            sw.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream(@"C:\diretorio\arquivo.txt", FileMode.OpenOrCreate,FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            string texto = sr.ReadToEnd();
            richTextBox1.Text = texto;
            sr.Close();
        }
    }
}
